1508-heteropar15-protobatsim
----------------------------

Repository to generate the `Communication Models Insights Meet Simulations` article
published at HeteroPar'2015 (Euro-Par workshop). https://hal.inria.fr/hal-01230288

How to build
------------

1. Install dependencies (latex suite, rubber, make)
2. Build figures: `(cd figures && make)`
3. Build pdf: `make`
