article:
	rubber -d main.tex

clean:
	rubber -d --clean main.tex

mrproper: clean
	rm -f main.pdf